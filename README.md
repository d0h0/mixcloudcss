# MixcloudCSS

Want a vertical playlist for the Mixcloud.com website? Use that css snippet!

## Usage in Firefox: 
### Option 1: With an Add-On that holds the CSS code
Various methods apply for adding custom styles to a website, e.g. by Add-Ons like userstyles, greasemonkey, tempermonkey, CustomCSS, ...
 - Copy the code to the addon

### Option 2: As global stylesheet
Such a stylesheet is applied to every website, but the CSS class selectors limit it to pages with that hierarchy and class names.
Append that code to 
 - Windows: C:\Users\<username>\AppData\Roaming\Mozilla\Firefox\Profiles\<profileId>.default\chrome\userContent.css
 - Linux: ~/.mozilla/firefox/profiles/<profileId>/chrome/userContent.css
 
